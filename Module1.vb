Imports System.IO
Imports System.Text

Module Module1

    'Loops through log file and check the archive file
    Function Main(ByVal argv() As String) As Integer

        If argv.Length <> 2 Then
            Console.WriteLine("LogChecker v1.0 - Lars Christian hegde, NTB" & vbCrLf)
            Console.WriteLine("Compares a logfile from the automated archiving module to an archive file")
            Console.WriteLine("from AiT, and writes the results to check-results.txt" & vbCrLf)
            Console.WriteLine("Usage:  LogChecker <logfile> <ait-archive>" & vbCrLf)
            'Return 1
        End If

        'Vars and streams
        Dim articleDelay = 60 ' Some articles are delayed for some reason, increase number (minutes) to increase match factor
        'Dim logFile As String = argv(0)
        Dim logFile As String = "c:\xmldocs\logs\2002-09-18-archived.txt" 'f:\ait-vrfy\mylogs\2002-09-12-archived.txt"
        'Dim archiveFile As String = argv(1)
        Dim archiveFile As String = "c:\xmldocs\ntb.data-242" 'f:\ait-vrfy\aitdata\ntb.data-255"
        'Dim resultFile As String = "check-results.txt"
        Dim resultFile As String = "f:\ait-vrfy\check-results.txt"

        Dim log_in As StreamReader
        Dim archive_in As StreamReader
        Dim result As StreamWriter

        Try
            log_in = New StreamReader(logFile, Encoding.GetEncoding("iso-8859-1"))
            archive_in = New StreamReader(archiveFile, Encoding.GetEncoding("iso-8859-1"))
            result = New StreamWriter(resultFile, False, Encoding.GetEncoding("iso-8859-1"))
        Catch
            Console.WriteLine("LogChecker v1.0 - Lars Christian hegde, NTB" & vbCrLf)
            Console.WriteLine("Input files not found." & vbCrLf)
            Console.WriteLine("Usage:  LogChecker <logfile> <ait-archive>" & vbCrLf)
            Return 1
        End Try

        Dim archive As ArrayList = New ArrayList()

        'Read the archive into an Arraylist
        Dim line As String
        Dim article As String

        While archive_in.Peek() > -1
            line = archive_in.ReadLine()

            If line.IndexOf("BRS DOCUMENT BOUNDARY") > -1 Then
                archive.Add(article)
                article = ""
            ElseIf line <> "" Then
                article += line & vbCrLf
            End If
        End While

        'If article <> "" Then archive.Add(article)
        archive_in.Close()

        'Read logfile and find articles missing in archive
        result.WriteLine("Missing in Archive:" & vbCrLf)

        line = log_in.ReadLine()
        Dim c As Integer = 0
        Dim a As String
        While line.IndexOf("Total archived") = -1

            Dim id As String = line.Split(vbTab)(1).Split("_")(1)
            Dim so As String = line.Split(vbTab)(4)
            Dim tm As DateTime = line.Split(vbTab)(2).Split(" ")(1)
            Dim f As Boolean = False

            For Each a In archive

                Dim at As DateTime = a.Substring(a.IndexOf("UT:") + 3, 5)
                Dim span = tm.Subtract(at)

                If a.IndexOf("SO:" & so) > -1 And a.IndexOf(id) > -1 And span.TotalMinutes <= articleDelay Then
                    f = True
                    Exit For
                End If
            Next

            If Not f Then
                result.WriteLine(line)
                c += 1
            Else
                archive.Remove(a)
            End If

            line = log_in.ReadLine()
        End While

        result.WriteLine(vbCrLf & "Count: " & c)

        result.WriteLine(vbCrLf & "Missing in Log:" & vbCrLf)

        For Each a In archive
            result.WriteLine(a)
        Next

        result.WriteLine(vbCrLf & "Count: " & archive.Count)
        result.WriteLine(vbCrLf & "Done!")
        result.Flush()

        Return 0
    End Function

End Module
